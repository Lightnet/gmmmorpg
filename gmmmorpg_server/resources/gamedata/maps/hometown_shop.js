/*
 * Project Name: gmmmorpg
 * Created by: Lightnet
 * licence: cc
 * Link: https://bitbucket.org/Lightnet/gmmmorpg
 */

exports.name = "home town shop";
exports.room = "rm_map_home_shop1";

exports.start_x = 240;
exports.start_y = 240;

exports.clients = [];
