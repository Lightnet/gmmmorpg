/*
 * Project Name: gmmmorpg
 * Created by: Lightnet
 * licence: cc
 * Link: https://bitbucket.org/Lightnet/gmmmorpg
 */

exports.name = "home town";
exports.room = "rm_map_home";

exports.start_x = 320;
exports.start_y = 320;

exports.clients = [];
