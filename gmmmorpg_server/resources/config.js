//import required lib
var args = require('minimist')(process.argv.slice(2));
var extend = require('extend');

// node server.js -env="test"
//store variables
var environment = args.env || "test";

console.log(environment);

//common config.. ie: name, version, max players etc...

var common_config = {
      name:"gm mmorpg server",
      version:"0.0.1",
      environment:environment,
      max_player:100,
      datapaths:{
            items:__dirname + "\\gamedata\\" + "items\\",
            maps:__dirname + "\\gamedata\\" + "maps\\"
      },
      starting_zone:"rm_map_home"
}

//--env="production" --ip=127.0.0.1

//environment specific configuration
var conf ={
      production:{
            ip: args.ip || "0.0.0.0",
            port: args.port || 8080,
            database: "mongodb://127.0.0.1/rm_mmo_prod"
      },
      test:{
            ip: args.ip || "0.0.0.0",
            port: args.port || 8082,
            database: "mongodb://127.0.0.1/rm_mmo_test"
      },
      alphatest:{
            ip: args.ip || "0.0.0.0",
            port: args.port || 8080,
            database: "mongodb://127.0.0.1/rm_mmo_alphatest"
      }
}

extend(false,conf.production,common_config);
extend(false,conf.test,common_config);

module.exports =  config = conf[environment];
