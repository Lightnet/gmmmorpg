/*
 * Project Name: gmmmorpg
 * Created by: Lightnet
 * licence: cc
 * Link: https://bitbucket.org/Lightnet/gmmmorpg
 */

var now = require('performance-now');
var _ = require('underscore');

module.exports = function(){
	var client = this;

	//Those objects will be added at run time
	//this.socket = {}
	//this.user = {}

	//initialization
	this.initiate = function(){
		//var client = this;
		//send the connection handshake packet to the client
		client.socket.write(packet.build(["HELLO",now().toString()]));
		console.log('client initiated');
	}

	//client methods
	this.enterroom = function(selected_room){
		maps[selected_room].clients.forEach(function(otherclient){
			otherclient.socket.write(packet.build(["POS",client.user.username,client.user.pos_x,client.user.pos_y]));
		});
		maps[selected_room].clients.push(client);
	}

	//get clients to current to users
	this.updateroom = function(){
		maps[client.user.current_room].clients.forEach(function(otherclient){
			if(client.user.username != otherclient.user.username ){
				client.socket.write(packet.build(["POS",otherclient.user.username,otherclient.user.pos_x,otherclient.user.pos_y]));
			}
		});
	}

	this.broadcastroom = function(packetdata){
		if (maps[client.user.current_room].clients.length > 0){
			maps[client.user.current_room].clients.forEach(function(otherclient){
				if(otherclient.user.username != client.user.username){
					otherclient.socket.write(packetdata);
				}
			});
		}
	}



    //Socket stuff
	this.data = function(data){
		//console.log("client data" + data.toString());
		packet.parse(client, data);
	}

	this.error = function(err){
		console.log("client error "+ err.toString());
	}

	this.end = function(){
		console.log("client closed");
		//client.save();
		//array.splice(index, 1);
		console.log("Clients:" + maps[client.user.current_room].clients.length);
		var clients = maps[client.user.current_room].clients;
		for(var i = 0; i < clients.length;i++){
			if(clients[i] == client){
				maps[client.user.current_room].clients.splice(i , 1);
			}
		}
		console.log("Current Clients:" + maps[client.user.current_room].clients.length);
		client.broadcastroom(packet.build(["LEFT",client.user.username]));
		client = null;
	}

}
