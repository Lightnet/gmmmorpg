/*
 * Project Name: gmmmorpg
 * Created by: Lightnet
 * licence: cc
 * Link: https://bitbucket.org/Lightnet/gmmmorpg
 * ref link: https://github.com/rm2kdev/Mmorpg-Server/blob/master/packet.js
 */

var zeroBuffer = new Buffer('00','hex');
var Parser = require('binary-parser').Parser;
var StringOptions = {length: 99, zeroTerminated: true};

module.exports = packet = {

	//params: an array of javascript object to be turned into buffers
	build:function(params){
		var packetParts = [];
		var packetSize = 0;

		params.forEach(function(param){
			  var buffer;

			  if(typeof param === 'string'){
					buffer = new Buffer(param,'utf8');
					buffer = Buffer.concat([buffer,zeroBuffer], buffer.length + 1);

			  }else if(typeof param === 'number'){
					buffer = new Buffer(2);
					buffer.writeUInt16LE(param,0);

			  }else{
					console.log("WARNING: unknown data type in packet builder!");
			  }
			  packetSize +=  buffer.length;
			  packetParts.push(buffer);

		});

		//SIZE -> DATA
		var dataBuffer =  Buffer.concat(packetParts,packetSize);
		var size = new Buffer(1);
		size.writeUInt8(dataBuffer.length + 1, 0);

		var finalPacket = Buffer.concat([size,dataBuffer],size.length + dataBuffer.length);

		return finalPacket;
	},

	//parse a packet to handled for a client
	parse:function(c, data){
		var idx = 0;
		while(idx < data.length){
			var packetSize = data.readUInt8(idx);
			var extractedPacket = new Buffer(packetSize);
			data.copy(extractedPacket, 0, idx, idx + packetSize);

			this.interpert(c, extractedPacket);

			idx += packetSize;
		}
	},
	
	interpert:function(c, datapacket){

		console.log("datapacket: ");
		console.log(datapacket);
		var header = PacketModels.header.parse(datapacket);
		console.log("-> Interpert: " + header.command.toUpperCase());

		switch (header.command.toUpperCase(datapacket)){
			case "LOGIN":
				var data = PacketModels.login.parse(datapacket);
				User.login(data.username,data.password,function(result, user){
					  console.log("Login Result:" + result);
					  if(result){
							c.user = user;
							c.enterroom(c.user.current_room);
							c.socket.write(packet.build(["LOGIN","TRUE", c.user.current_room, c.user.pos_x, c.user.pos_y, c.user.username]));
							//c.updateroom(); //doesn't work for some reason that loading the user first than last trigger since the room just build.
					  }else{
							c.socket.write(packet.build(["LOGIN","FALSE"]));
					  }
				});

				break;

			case "REGISTER":
				console.log("REGISTER?");
				var data = PacketModels.register.parse(datapacket);
				console.log(User.register);

				User.register(data.username, data.password,function(result){
					if(result){
						c.socket.write(packet.build(["REGISTER","TRUE"]));
					}else{
						c.socket.write(packet.build(["REGISTER","FALSE"]));
					}
				});
				break;

			case "POS":
				var data = PacketModels.pos.parse(datapacket);
				console.log(data);
				c.user.pos_x = data.target_x;
				c.user.pos_y = data.target_y;
				c.user.save();
				c.broadcastroom(packet.build(["POS",c.user.username, data.target_x, data.target_y]));

				break;
				
			case "UPDATEROOM":
				var data = PacketModels.updateroom.parse(datapacket);
				console.log(data);
				c.updateroom();//when room is loaded it get triggered.
				
				break;

		}
	}
}
