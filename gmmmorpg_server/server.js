/*
 * Project Name: gmmmorpg
 * Created by: Lightnet
 * licence: cc
 * Link: https://bitbucket.org/Lightnet/gmmmorpg
 */

//import required libs
require(__dirname + "/resources/config.js")
var fs = require('fs');
var net = require('net');
require('./packet.js');
//console.log(config.database);
//load initalizers
var init_files = fs.readdirSync(__dirname + "/initalizers");
init_files.forEach(function(initFile){
      console.log('loading initalizer: ' + initFile);
      require(__dirname + "/initalizers/" + initFile);
});

//load models
var model_files = fs.readdirSync(__dirname + "/models");
model_files.forEach(function(modelFile){
      console.log('loading model: ' + modelFile);
      require(__dirname + "/models/" + modelFile);
});

//load maps
maps = {}
var map_files = fs.readdirSync(config.datapaths.maps);
map_files.forEach(function(mapFile){
      console.log('loading Map: ' + mapFile);
      var map = require(config.datapaths.maps + mapFile);
      maps[map.room] = map;
});

net.createServer(function(socket){
	console.log("socket connected");

	var c_init = new require('./client.js');
	var thisClient = new c_init();

	thisClient.socket = socket;
	thisClient.initiate();

	socket.on('error',thisClient.error);
	socket.on('data',thisClient.data);
	socket.on('end',thisClient.end);

}).listen(config.port);

console.log("initalize completed, server runnning on port: " + config.port + " for enviroment:" + config.environment);

//console.log(maps);

//maps.rm_start_town

//1. load initalizers
//2. load data models
//3. load game map data
//4. initiate the server and listen to the internets
      //add of the server logic
