/*
 * Project Name: gmmmorpg
 * Created by: Lightnet
 * licence: cc
 * Link: https://bitbucket.org/Lightnet/gmmmorpg
 */

var mongoose = require('mongoose');

module.exports = gamedb = mongoose.createConnection(config.database);
