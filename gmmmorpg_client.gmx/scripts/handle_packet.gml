//argument0: data buffer
var command = buffer_read(argument0,buffer_string);
show_debug_message("Networking Event: " +  string(command));

switch(command){

    case "HELLO":
        server_time = buffer_read(argument0,buffer_string);
        room_goto_next();
        show_debug_message("Server welcome you @ " + server_time);
        break;
        
    case "LOGIN":
        status  = buffer_read(argument0, buffer_string);
        if (status == "TRUE"){
        
             target_room = buffer_read(argument0, buffer_string);
             target_x = buffer_read(argument0, buffer_u16);
             target_y = buffer_read(argument0, buffer_u16);
             name = buffer_read(argument0, buffer_string);
             
             goto_room = asset_get_index(target_room);
             room_goto(goto_room);
             
             //init a player object... on this room.
             with(instance_create(target_x,target_y,obj_Player)){
                name = other.name;
             }
             
             UpdateRoom_packet = buffer_create(1,buffer_grow,1);
             buffer_write(UpdateRoom_packet,buffer_string,"UpdateRoom");

             network_write(Network.socket,UpdateRoom_packet);
        
        }else{
            show_message("Login Failed: Username Doesn't Exist or Password Incorrect.");        
        }
    
        break;
        
    case "REGISTER":
        status  = buffer_read(argument0, buffer_string);
        if (status == "TRUE"){
            show_message("Register Success: Please login.");
        }else{
            show_message("Register Failed: Username Taken.");        
        }
    
        break;
        
    case "POS":
        show_debug_message("incoming...");
        username  = buffer_read(argument0, buffer_string);
        target_x  = buffer_read(argument0, buffer_u16);
        target_y  = buffer_read(argument0, buffer_u16);
        
        foundPlayer = -1;
        with(obj_Network_Player){
        
            if(name == other.username){
                other.foundPlayer = id;
                break;
            }
        }
        
        if (foundPlayer != -1){
        
            with(foundPlayer){
            target_x = other.target_x;
            target_y = other.target_y;
            
            }
        
        }else{
        
            with(instance_create(target_x, target_y, obj_Network_Player)){
                name = other.username;
            }
        }
        break;
    case "LEFT":
        username  = buffer_read(argument0, buffer_string);
        show_debug_message("User Left@: " + username +  " ");
        
        foundPlayer = -1;
        with(obj_Network_Player){
        
            if(name == other.username){
                //remove client when player leave.
                instance_destroy();
                break;
            }
        }
        
        break;        

}
